#!/bin/node
let child = require('child_process');
let fs = require("fs");

//without asm extension
var nasmFiles = ['stage1','stage2'];
var outName = "image.bin";
var tmpPath = "tmp/";

function compileNasm(fileName, outFile){
  var proc = child.execSync('nasm ' + fileName + ".asm -o " + outFile);
  if(proc.stderr != null)
  {
    console.log("[ASM] " + fileName + " Failed to build!\n" + proc.stderr.toString());
    process.exit();
  }
  console.log("[ASM] " + fileName + " Compiled");
}

function concatFiles(prefixPath, nasmFiles, outName){
  //create buff array, and calculate length
  var buffers = [];
  var totalLength = 0;
  for(var i = 0; i < nasmFiles.length;i++){
    var temp = fs.readFileSync(prefixPath + nasmFiles[i]);
    buffers.push(temp);
    totalLength += temp.length;

  }
  //create buff
  var buff = Buffer.concat(buffers,totalLength);
  //save it
  fs.writeFile(outName, buff, function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The floppy image was builded!\nFile name: " + outName);
  });
}



//compile asm files
for(var i = 0; i < nasmFiles.length; i++){
  compileNasm(nasmFiles[i],tmpPath + nasmFiles[i]);
}
//concat to image
concatFiles(tmpPath,nasmFiles, outName);
