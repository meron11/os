[bits 16]
[org 0x0000]

main:
  mov ax, 0x3000
  mov ds,ax
  mov es,ax
  ; cleanScreen:
  ; mov dx,0xFFFF
  ; mov cx,0x0000
  ; mov ah, 07h ; set interrupt to scrool down
  ; mov al, 0h ; clear screen
  ; int 10h

  ;enable a20
  in al, 0x92
  or al, 2
  out 0x92, al

  ;LOAD GDT
  lgdt [GDT_addr]
  ;set pagging
  mov eax, cr0
  or al, 1
  mov cr0, eax
  ; jmp $
  jmp dword 0x8:(0x30000 + start32)

start32:
  [bits 32]
  mov ax, 0x10
  mov ds, ax
  mov es, ax
  mov ss, ax

  lea eax, [0xb8000]

  mov dword [eax], 0x9F759F64
  lea eax, [0xb8000 + 0x4];
  mov dword [eax], 0x9F619F70
  jmp $



  GDT_addr:
  dw (GDT_end - GDT) - 1
  dd 0x30000 + GDT

GDT:
    ;null
    dd 0x00
    dd 0x00

    ;code segment
    dd 0x0000FFFF ; numer of limit addresses in segment
    dd 0x00CF9A00 ; @see intel mannual page 2063, tl;dr set segment as 32 bit, present set to 1, code or data

    ;data segment
    dd 0xFFFF
    dd 0x00CF9300

    ;null
    dd 0x00
    dd 0x00

GDT_end:

times 1024 - ($ - $$) nop
