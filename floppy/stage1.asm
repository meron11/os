[bits 16] ; cpu starts at 8086 mode
[org 0x7c00] ; bios load here!

jmp word 0x0000:start ;to be sure set segment at beggining and jump to start label
start:
  ; cleanScreen:
  ; mov ah, 07h ; set interrupt to scrool down
  ; mov al, 0h ; clear screen
  ; mov dh, 25 ;row
  ; mov dl, 80 ; cols
  ; int 10h

  mov ax, 0x3000 ; 0x3000:0x0000
  mov es, ax
  xor bx, bx ; bx == 0
  ;read next code from disk to memory to address [es:bx] = 0x30000:0x0000
  mov ah, 2
  mov al, 2   ;how much sectors? sector = 512 bytes
  mov ch, 0 ;always in diskiette
  mov cl, 2 ; starts from second sector, this is first!;)
  mov dh,0
  int 13h

  ;jmp to loaded code
  jmp word 0x3000:0x000;


; to fill
epilogue:
  %if ($ - $$) > 510
    %fatal "Bootloader code exceed 512 bytes."
  %endif

  times 510 - ($ - $$) db 0
  db 0x55
  db 0xAA
